package com.example.jochen.odiseephotobooth.fontchanger;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

/**
 * Class to change the font of a RadioButton.
 *
 * @author Lorenzo Longo
 * @version 29/04/2017
 */
public class FontChangerRadioButton extends android.support.v7.widget.AppCompatRadioButton {

    /**
     * Constructor to change the font of a RadioButton.
     *
     * @param c Access Context class.
     * @param s Access AttributeSet class.
     */
    public FontChangerRadioButton(Context c, AttributeSet s) {
        super(c, s);
        this.setTypeface(Typeface.createFromAsset(c.getAssets(), "Fonts/AvenirNextLTPro-Bold.otf"));
    }
}
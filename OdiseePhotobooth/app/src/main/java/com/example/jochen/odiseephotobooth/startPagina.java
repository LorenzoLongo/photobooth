package com.example.jochen.odiseephotobooth;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 *
 * Main class which provides the user interaction.
 * User can choose to go to the camera page
 * @author Duyck Jochen
 * @version 13/05/2017
 */

public class startPagina extends AppCompatActivity {
    TextView textview;
    Button button;

    /**
     * Main method of the form.
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_startpagina);
        button = (Button) findViewById(R.id.startbutton);
        textview = (TextView)findViewById(R.id.text_view);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            //go to the next page
            public void onClick(View v) {
                Intent intent = new Intent("com.example.jochen.odiseephotobooth.CameraActivity");
                startActivity(intent);
            }
        });
    }

    /**
     * full screen mode
     * @param hasFocus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if(hasFocus) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }
}

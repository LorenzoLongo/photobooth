package com.example.jochen.odiseephotobooth.fontchanger;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Class to change the font of a button.
 *
 * @author Lorenzo Longo
 * @version 30/04/2017
 */
public class FontChangerButton extends android.support.v7.widget.AppCompatButton {

    /**
     * Constructor to change the font of a button.
     *
     * @param c Access Context class.
     * @param s Access AttributeSet class.
    */
    public FontChangerButton (Context c, AttributeSet s) {
        super(c, s);
        this.setTypeface(Typeface.createFromAsset(c.getAssets(), "Fonts/AvenirNextLTPro-Bold.otf"));
    }
}
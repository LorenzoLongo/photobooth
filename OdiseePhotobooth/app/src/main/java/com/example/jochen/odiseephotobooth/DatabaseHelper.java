package com.example.jochen.odiseephotobooth;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Class which creates the student database and insert user input values.
 *
 * @author Lorenzo Longo
 * @version 29/04/2017
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    //Creates database and sets table name variables
    public static final String DATABASE_NAME = "Studentengegevens.db";
    public static final String TABLE_NAME = "student_table";

    //Creates table variables/values
    public static final String ID = "ID";
    public static final String SURNAME = "SURNAME";
    public static final String NAME = "NAME";
    public static final String EMAIL = "EMAIL";
    public static final String EDUCATION = "EDUCATION";

    /**
     * Initializing constructor.
     *
     * @param context Access Context class.
     */
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    /**
     * Method which creates the student database.
     *
     * @param db Access SQLiteDatabase class.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, SURNAME TEXT, EMAIL TEXT, EDUCATION TEXT)");
    }

    /**
     * Method which recreates database if necessary.
     *
     * @param db Access SQLiteDatabase class.
     * @param oldVersion Old version of the database.
     * @param newVersion New version of the database.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    /**
     * Method which inserts the data a user puts in.
     *
     * @param name Inserted name value.
     * @param surname Inserted surname value.
     * @param email Inserted email value.
     * @param education Inserted education choice.
     *
     * @return Data inserted true or false.
     */
    public boolean insertData(String name, String surname, String email, String education) {
        SQLiteDatabase db = this.getWritableDatabase();

        //Insert content values for every table variable.
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME, name);
        contentValues.put(SURNAME, surname);
        contentValues.put(EMAIL, email);
        contentValues.put(EDUCATION, education);

        //Check if values are inserted.
        long result = db.insert(TABLE_NAME, null, contentValues);

        if(result == -1) {
            return false;
        }
        else {
            return true;
        }
    }

}

package com.example.jochen.odiseephotobooth;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.os.Bundle;
import android.widget.ImageView;


import java.io.File;


/**
 *
 * Main class which provides the user interaction.
 * This page displays the recent taken picture.
 * The user can choose to go back and take a new picture or proceed to the next page to complete the form.
 * @author Duyck Jochen
 * @version 13/05/2017
 */

public class FotoPreview extends AppCompatActivity implements View.OnClickListener {
    Button buttonNext;
    Button buttonBack;

    /**
     * Main method of the form.
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto_preview);
        buttonNext = (Button) findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(this);
        buttonBack = (Button) findViewById(R.id.buttonBack);
        buttonBack.setOnClickListener(this);
        ImageView imageView = (ImageView) findViewById(R.id.imageView4);
        String imageLocation = getLastTakenPicture();
        File imageFile = new File(imageLocation);
        if (imageFile.exists()) {
            Bitmap bm = BitmapFactory.decodeFile(imageLocation);
            imageView.setImageBitmap(bm);
        }
    }

    /**
     * if buttonBack is clicked return to CameraActivity to take a new picture
     * if buttonNext is clicked go to Form (Form_DataBase)
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonBack:
                Intent intent = new Intent("com.example.jochen.odiseephotobooth.CameraActivity");
                startActivity(intent);
                /*
                * delete latest taken picture
                */
                String imageLocation = getLastTakenPicture();
                File file = new File(imageLocation);
                file.delete();
                break;

            case R.id.buttonNext:
                Intent intent2 = new Intent("com.example.jochen.odiseephotobooth.Form_Database");
                startActivity(intent2);
                break;

            default:
                break;
        }
    }

    /**
     * full screen mode
     * @param hasFocus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    /**
     * Get the last taken picture and display it in a imageView
     */
    public String getLastTakenPicture() {
        // Find the last picture
        String[] projection = new String[]{MediaStore.Images.ImageColumns._ID, MediaStore.Images.ImageColumns.DATA, MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, MediaStore.Images.ImageColumns.DATE_TAKEN, MediaStore.Images.ImageColumns.MIME_TYPE};
        final Cursor cursor = getApplicationContext().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");
        // Put it in the image view
        String imageLocation = null;
        if (cursor.moveToFirst()) {
            imageLocation = cursor.getString(1);
        }
        return imageLocation;
    }
}

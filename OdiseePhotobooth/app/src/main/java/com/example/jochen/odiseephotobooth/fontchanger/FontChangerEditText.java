package com.example.jochen.odiseephotobooth.fontchanger;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Class to change the font of a EditText design.
 *
 * @author Lorenzo Longo
 * @version 29/04/2017
 */
public class FontChangerEditText extends android.support.v7.widget.AppCompatEditText {

    /**
     * Constructor to change the font of a EditText design.
     *
     * @param c Access Context class.
     * @param s Access AttributeSet class.
     */
    public FontChangerEditText (Context c, AttributeSet s) {
        super(c, s);
        this.setTypeface(Typeface.createFromAsset(c.getAssets(), "Fonts/AvenirNextLTPro-Bold.otf"));
    }
}

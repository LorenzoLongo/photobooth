package com.example.jochen.odiseephotobooth;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.provider.MediaStore;
import android.support.v4.print.PrintHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Main class which provides the user interaction. This user interaction goes from inserting personal data from a form and sending an automatic mail.
 *
 * @author Lorenzo Longo
 * @version 29/04/2017
 */
public class Form_Database extends AppCompatActivity implements View.OnClickListener{
    //Creates object from DatabaseHelper class
    DatabaseHelper myDb;

    //Declaring variables
    int selectedId;

    TextView textError;
    RadioGroup studyChoice;
    RadioButton editEducation;
    EditText editName, editSurname, editEmail;
    Button btnAddData, btnBack;


    /**
     * Main method of the form. Which runs the program form.
     *
     * @param savedInstanceState Access Bundle class.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Hides the action bar of the form which provides full screen mode
        getSupportActionBar().hide();
        setContentView(R.layout.activity_form__database);

        myDb = new DatabaseHelper(this);

        //Sets variable to group of radio buttons from xml file
        studyChoice = (RadioGroup)findViewById(R.id.radioButtonChoice);

        //Sets variables to edible text lines from xml file
        editName = (EditText)findViewById(R.id.edit_Name);
        editSurname = (EditText)findViewById(R.id.edit_Surname);
        editEmail = (EditText)findViewById(R.id.edit_Email);

        textError = (TextView)findViewById(R.id.contact_form_description);

        //Sets variable to button from xml file
        btnAddData = (Button)findViewById(R.id.btnSend);

        btnBack = (Button) findViewById(R.id.buttonStartPag);
        btnBack.setOnClickListener(this);
        //Call addData method
        addData();

    }

    /**
     * Method where the user input will be transferred to the database created in the DatabaseHelper class.
     * This method also sends an automatic mail, with the use of the SendMail class and checks for wrong user inputs.
     *
     */
    private void addData() {
        btnAddData.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Provides errormessages
                        if( editName.getText().toString().length() < 2 ) {
                            editName.setError(Html.fromHtml("<font color='#D26428'>Naam moet tenminste 2 letters bevatten!</font>"));
                        }
                        else if(editSurname.getText().toString().length() < 2) {
                            editSurname.setError(Html.fromHtml("<font color='#D26428'>Voornaam moet tenminste 2 letters bevatten!</font>"));
                        }
                        else if(editEmail.getText().toString().length() < 5 || !editEmail.getText().toString().contains("@")) {
                            editEmail.setError(Html.fromHtml("<font color='#D26428'>Geef een geldig mailadres in!</font>"));
                        }
                        else if(editName.getText().toString().toUpperCase().contains("DROP") || editSurname.getText().toString().toUpperCase().contains("DROP") || editEmail.getText().toString().toUpperCase().contains("DROP")) {
                            textError.setError(Html.fromHtml("<font color='#D26428'>SQL Injectie!</font>"));
                        }
                        else if(editName.getText().toString().toUpperCase().contains("DELETE") || editSurname.getText().toString().toUpperCase().contains("DELETE") || editEmail.getText().toString().toUpperCase().contains("DELETE")) {
                            textError.setError(Html.fromHtml("<font color='#D26428'>SQL Injectie!</font>"));
                        }
                        else if(editName.getText().toString().toUpperCase().contains("INSERT") || editSurname.getText().toString().toUpperCase().contains("INSERT") || editEmail.getText().toString().toUpperCase().contains("INSERT")) {
                            textError.setError(Html.fromHtml("<font color='#D26428'>SQL Injectie!</font>"));
                        }
                        else {
                            //Gets the by user selected radiobutton
                            selectedId = studyChoice.getCheckedRadioButtonId();
                            editEducation = (RadioButton)findViewById(selectedId);

                            //Inserts data from user input
                            boolean isInserted = myDb.insertData(editName.getText().toString().toUpperCase(), editSurname.getText().toString().toUpperCase(), editEmail.getText().toString(), editEducation.getText().toString());

                            //Calls sendEmail method
                            sendEmail();

                            //Checks if user input is inserted successfully
                            if(isInserted == true) {
                                Toast.makeText(Form_Database.this,"Gegevens toegevoegd",Toast.LENGTH_LONG).show();
                                doPhotoPrint();
                                //sendFile(ImagePath);
                            }
                            else {
                                Toast.makeText(Form_Database.this,"Gegevens niet toegevoegd",Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
        );
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonStartPag:
                Intent intent = new Intent(Form_Database.this, startPagina.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            default:

                break;
        }
    }
    /**
     * Method which creates a bitmap of the picture with the Odisee logo.
     *
     */
    private void doPhotoPrint() {
        try {
            //Creates object from PrintHelper class
            PrintHelper photoPrinter = new PrintHelper(this);

            //Scales image
            photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FILL);

            // Find the last picture
            String[] projection = new String[]{
                    MediaStore.Images.ImageColumns._ID,
                    MediaStore.Images.ImageColumns.DATA,
                    MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                    MediaStore.Images.ImageColumns.DATE_TAKEN,
                    MediaStore.Images.ImageColumns.MIME_TYPE};
            final Cursor cursor = getApplicationContext().getContentResolver()
                    .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null,
                            null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

            String imageLocation = null;

            if (cursor.moveToFirst()) {
                imageLocation = cursor.getString(1);

            }
            File imageFile = new File(imageLocation);
            ImagePath = imageLocation;
            //Creates bitmapfile to print
            if (imageFile.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;

                //Gets the resources (last taken picture + Odisee logo)
                Bitmap bitmap = BitmapFactory.decodeFile(imageLocation);
                Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(),R.drawable.logo_odisee);
                Bitmap bmOverlay = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());

                //Draws the Odisee logo onto the picture
                Canvas canvas = new Canvas(bmOverlay);
                canvas.drawBitmap(bitmap, new Matrix(),null);
                canvas.drawBitmap(bitmap2,900,650,null);

                //Sets printer orientation to landscape
                photoPrinter.setOrientation(PrintHelper.ORIENTATION_LANDSCAPE);

                //Goes to screen to print the bitmap
                photoPrinter.printBitmap("photobooth", bmOverlay);
            }
        }catch (Exception e) {
            editName.setError("" + e);
        }

    }

    /**
     * Sets the content for the mail, creates object for the SendMail class and sends the mail.
     *
     */
    private void sendEmail() {
        //Sets content for the mail
        String email = editEmail.getText().toString();
        String subject = "Photobooth Odisee";
        String message = "Hallo " + editSurname.getText().toString().substring(0,1).toUpperCase() + editSurname.getText().toString().substring(1) + "!\n\nBedankt om gebruik te maken van onze photobooth!\nBent u geïnteresseerd in een van onze opleidingen? Neem dan snel een kijkje op:\n\n\thttps://www.odisee.be/\n\nLike ons nu op Facebook!\n\n\thttps://nl-nl.facebook.com/odiseehogeschool/\n\nOdisee Hogeschool\nEen verhaal dat jij schrijft!";

        //Creates object from SendMail class
        SendMail sm = new SendMail(this, email, subject, message);

        //Sends mail to mailaddress from user input
        sm.execute();
    }




    /********************************************************************
     *
     */

    private String addr = "192.168.1.10";
    private int port= 8000;
    Socket socket;
    String ImagePath;

    /**
     * sendPhoto brings all the methods to send a photo to a host together
     *
     */
    public void sendPhoto(String path){
        openConnection();
        sendFile(path);
        closeConnection();
    }

    /**
     * sendFile will try to send the file to the host
     *
     * @param imagepath path to the img
     */
    private void sendFile(String imagepath){
        try{
            //the file is read, the size of it is calculated (in bytes)
            File Photo = new File (imagepath);
            byte [] bytearray  = new byte [(int)Photo.length()];
            FileInputStream fileinptstrm = new FileInputStream(Photo);

            BufferedInputStream bufinptstrm = new BufferedInputStream(fileinptstrm);
            bufinptstrm.read(bytearray,0,bytearray.length);

            //the file (in a bytearray) is sent
            OutputStream otptstrm = socket.getOutputStream();
            otptstrm.write(bytearray,0,bytearray.length);
            otptstrm.flush();

        } catch (UnknownHostException e) {
            //an exception for an unknown ip address from the host will be caught here & printed
            e.printStackTrace();
        } catch (IOException e) {
            //an exception for an unknown input/output will be caught here & printed
            e.printStackTrace();
        }
    }

    /**
     * openConnection will try to open the connection
     *
     */
    private void openConnection(){
        try {
            InetAddress address = InetAddress.getByName(addr);
            socket = new Socket(address,port);

        } catch (UnknownHostException e) {
            //an exception for an unknown ip address from the host will be caught here & printed
            e.printStackTrace();
        } catch (IOException e) {
            //an exception for an unknown input/output will be caught here & printed
            e.printStackTrace();
        }
    }

    /**closeConnection will close the
     *
     */
    private void closeConnection(){
        try{
            socket.close();

        } catch (IOException e) {
            //an exception for an unknown input/output will be caught here & printed
            e.printStackTrace();
        }
    }

}

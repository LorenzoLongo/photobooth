package com.example.jochen.odiseephotobooth.fontchanger;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Class to change the font of a TextView design.
 *
 * @author Lorenzo Longo
 * @version 29/04/2017
 */
public class FontChangerTextView extends android.support.v7.widget.AppCompatTextView {

    /**
     * Constructor to change the font of a TextView design.
     *
     * @param c Access Context class.
     * @param s Access AttributeSet class.
     */
    public FontChangerTextView(Context c, AttributeSet s) {
        super(c, s);
        this.setTypeface(Typeface.createFromAsset(c.getAssets(), "Fonts/AvenirNextLTPro-Bold.otf"));
    }

}


package com.example.jochen.odiseephotobooth;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.widget.Toast;

import java.io.File;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Class which creates the mailobject and makes the connection with Gmail servers.
 *
 * @author Lorenzo Longo
 * @version 29/04/2017
 */
public class SendMail extends AsyncTask<Void,Void,Void> {

    //Creating object from Context and Session classes
    private Context context;
    private Session session;

    //Declaring variables
    private String email;
    private String subject;
    private String message;

    //Variable to show the process while sending a mail
    private ProgressDialog progressDialog;

    /**
     * Initializing constructor.
     *
     * @param context Access Context class.
     * @param email Variable email address.
     * @param subject Variable subject.
     * @param message Variable message.
     */
    public SendMail(Context context, String email, String subject, String message){
        //Initializing variables
        this.context = context;
        this.email = email;
        this.subject = subject;
        this.message = message;
    }

    /**
     * Method which sets up the task to send a mail and show the progress to the user inside the GUI.
     *
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        //Showing progress dialog while sending mail
        progressDialog = ProgressDialog.show(context,"Bericht wordt verzonden","Even geduld aub...",false,false);
    }

    /**
     * Gettermethod to get the subject.
     *
     * @return Subject of the mail.
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Gettermethod to get the message.
     *
     * @return Message of the mail.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Method which shows the user that the message is sent succesfully.
     *
     * @param aVoid Access Void class.
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        //Closing the progressbar inside the GUI
        progressDialog.dismiss();

        //Opening a success message inside the GUI
        Toast.makeText(context,"Bericht verzonden",Toast.LENGTH_LONG).show();
    }

    /**
     * Method which configures gmail properties and creates mailobject.
     *
     * @param params Access Void class.
     * @return Null.
     */
    @Override
    protected Void doInBackground(Void... params) {
        //Creating properties
        Properties props = new Properties();

        //Configuring properties for gmail
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        //Login with gmail account
        session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {

                    //Authentication
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(ConfigMail.EMAIL, ConfigMail.PASSWORD);
                    }
                });

        //Tries creating new MimeMessage object
        try {
            //Creating MimeMessage object
            MimeMessage mm = new MimeMessage(session);

            //Sets sender address
            mm.setFrom(new InternetAddress(ConfigMail.EMAIL));

            //Adds receiver address
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress(email));

            //Sets subject
            mm.setSubject(subject);

            //Sets content message
            mm.setText(message);

            //Sends mail
            Transport.send(mm);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }

}


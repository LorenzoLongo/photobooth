= Logboek Project Photobooth

|===

|#*Februari*# | 

.4+|*09/02*
|Openen logboek -> #*Jochen Duyck*#

|Uurroosters samenvoegen -> #*Groep*#

|Mail eerste afspraak met begeleider [Rogier van der Linde] -> #*Lorenzo Longo*#

|Controle mail -> #*Groep*#

.6+|*13/02*
|Opstellen planning -> #*Jochen Duyck*# - #*Jakob Steurtewagen*#

|Links mogelijke hardwarecomponenten verzamelen -> #*Jochen Duyck*#

|GIT aanmaken -> #*Jochen Duyck*#

|Inleiding Wetenschappelijk Rapporteren -> #*Jochen Duyck*# - #*Jakob Steurtewagen*#

|Uitwerking en concretisering van het projectvoorstel -> #*Jochen Duyck*# - #*Lorenzo Longo*#

|Tijdlijn planning vormgeven -> #*Lorenzo Longo*#

.2+|*16/02*
|Logboek aanmaken GIT -> #*Lorenzo Longo*#

|Uitprinten bundel (Concretisering, tijdlijn, planning, inleiding) -> #*Lorenzo Longo*#

|*20/02* |Zoeken geschikte tablet -> #*Lorenzo Longo*#

.3+|*21/02*
|Logboek bewerken en updaten -> #*Lorenzo Longo*#

|Research creëren applicatie in android + ontwikkeling filters in applicatie -> #*Lorenzo Longo*#

|Aanvullen Inleinding (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

.2+|*22/02* 
|Aanvullen titels tot niveau 2 (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

|Inleiding mailen naar Sabine Martens (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*# 

|#*Maart*# | 

.3+|*02/03* 
|Meeting - opzoeken hardwarecomponenten -> #*Jochen Duyck*# - #*Lorenzo Longo*#

|Kostenberekening -> #*Jochen Duyck*# - #*Lorenzo Longo*#

|Mailen Peter Demeester -> #*Jochen Duyck*# - #*Lorenzo Longo*#

|*05/03* |Verbeteren tekst inleiding photobooth (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

|*06/03* |Verbeteren titels tot niveau 2 (Wetenschappelijk rapporteren) -> #*Jochen Duyck*# - #*Lorenzo Longo*# - #*Jakob Steurtewagen*#

|*10/03* |Mailen Rogier van der Linde om afspraak -> #*Lorenzo Longo*#

.4+|*11/03* 
|Bepalen programmeertalen & software die zal worden gebruikt -> #*Jochen Duyck*# - #*Jakob Steurtewagen*# - #*Lorenzo Longo*#

|Aanpassen titels tot niveau 2 (Wetenschappelijk rapporteren) ->  #*Jochen Duyck*# - #*Lorenzo Longo*#

|Verdeling subhoofdstukken H1 (Wetenschappelijk rapporteren) -> #*Groep*#

|Schema maken hoe de applicatie zal werken -> #*Lorenzo Longo*#

.6+|*15/03*
|Meeting -> #*Groep*#

|Brainstormen behuizing photobooth, hoe maken, materiaalkeuze -> #*Groep*#

|Behuizing afmetingen bepalen -> #*Groep*#

|Schetsen photobooth -> #*Jochen Duyck*# - #*Lorenzo Longo*#

|Mailen Ans Mouton/Rogier van der Linde -> #*Jochen Duyck*# - #*Lorenzo Longo*#

|Afwerken tekst subtitel (Wetenschappelijk rapporteren) -> #*Michiel Goethals*# - #*Jakob Steurtewagen*#

.2+|*16/03* 

|Connectie tussen hardwarecomponenten researchen (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

|H1 schrijven - subtitel: Connectie hardwarecomponenten (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

.2+|*17/03*
|Mailen H1 - Mogelijk oplossingen (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

|Meeting Rogier van der Linde -> #*Groep*#

.6+|*18/03 - 20/03*
|Prototype maken -> #*Lorenzo Longo*#

|Snijden van karton op juiste maat -> #*Lorenzo Longo*#

|Extra versterkende laag krantenpapier lijmen -> #*Lorenzo Longo*#

|Schilderen prototype -> #*Lorenzo Longo*#

|Verstevigen prototype -> #*Lorenzo Longo*#

|Printerbak maken -> #*Lorenzo Longo*#

.3+|*21/03* 
|Mailen Ans Mouton + Rogier van der Linde (foto's prototype) - #*Lorenzo Longo*#

|Suggesties verbetering H1 (Wetenschappelijk rapporteren) -> #*Jochen Duyck*#

|Verdeling subhoofdstukken H2 (Wetenschappelijk rapporteren) -> #*Groep*#

.2+|*22/03*
|Meeting -> #*Jochen Duyck*# - #*Lorenzo Longo*#

|Researchen H2 (Wetenschappelijk rapporteren) -> #*Jochen Duyck*# - #*Lorenzo Longo*#

|*24/03* |Verbeteren H1 (Wetenschappelijk rapporteren) -> #*Jochen Duyck*# - #*Lorenzo Longo*# - #*Michiel Goethals*#

.3+|*26/03*
|Researchen info proof of concept -> #*Lorenzo Longo*#

|Mailen Rogier van der Linde - Verdere info rond proof of concept verkrijgen -> #*Lorenzo Longo*#

|Mailen Thomas Lenoir - Afspraak maken met OPT voor behuizing photobooth -> #*Lorenzo Longo*#

|#*April*# | 

|*04/04* |Updaten logboek -> #*Lorenzo Longo*#

|*06/04* - *16/04*|Zelfstudie Android Studio -> #*Lorenzo Longo*#

|*13/04* - *15/04*|Schrijven H2 (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

.3+|*19/04* 
|Samenzetten H2 (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

|Laatste verbeteringen document (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

|Verzenden H2 Mevr. Sabine Martens (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

.2+|*20/04* - *22/04*
|Researchen geautomatiseerde mail -> #*Lorenzo Longo*#

|Researchen opbouwen formulier in Android Studio -> #*Lorenzo Longo*#

.2+|*23/04*
|Research Databases in Android -> #*Lorenzo Longo*#

|Uittesten databases in Android Studio -> #*Lorenzo Longo*#

|*24/04* |Verder researchen en testen databases -> #*Lorenzo Longo*#

.3+|*25/04* 
|Programmeren formulier in Android Studio -> #*Lorenzo Longo*#

|Programmeren geautomatiseerde mail -> #*Lorenzo Longo*#

|Programmeren database in formulier -> #*Lorenzo Longo*#

.2+|*26/04* 
|Feedback (Wetenschappelijk rapporteren) -> #*Groep*#

|Teamgesprek (update applicatie) -> #*Groep*#

|*29/04* |Logo's ontwerpen voor applicatie -> #*Lorenzo Longo*#

.4+|*30/04* 
|SQL injectie bescherming programmeren -> #*Lorenzo Longo*#

|Email en naamcontrole programmeren -> #*Lorenzo Longo*#

|Helpen verbetering programmatie andere delen -> #*Lorenzo Longo*#

|Layout applicatie verbeteren -> #*Lorenzo Longo*#

|#*Mei*# |

.3+|*03/05* 
|Teamgesprek (update applicatie) -> #*Groep*#

|Samenstellen applicatie (deel 1) -> #*Groep*#

|Applicatie testen op tablet -> #*Groep*#

|*08/05* ||Researchen en testen app automatisch naar startpagina zonder te sluiten (deel 1) -> #*Lorenzo Longo*#

|*09/05* |Samenstellen applicatie (deel 2) -> #*Groep*#

.3+|*10/05* 
|Verbeteren layout applicatie -> #*Lorenzo Longo*#

|Verbeteren layout code -> #*Lorenzo Longo*#

|Commentaar bij code updaten -> #*Lorenzo Longo*#

|*11/05* |Mailen Mnr. van der Linde (proof of concept applicatie) -> #*Lorenzo Longo*#

.6+|*12/05* 
|Mailen Mnr. van der Linde -> #*Lorenzo Longo*#

|Reasearch hardwarecomponenten (voor bestelling) -> #*Lorenzo Longo*#

|Mailen Mnr. Mouton (hoe bestellen) -> #*Lorenzo Longo*#

|Mailen Mnr. Mouton (kostenraming bestelling) -> #*Lorenzo Longo*#

|Mailen Mnr. Mouton (extra info hardware) -> #*Lorenzo Longo*#

|Researchen en testen app automatisch naar startpagina zonder te sluiten (deel 2) -> #*Lorenzo Longo*#

|*13/05* |Researchen en testen app automatisch naar startpagina zonder te sluiten (deel 3) -> #*Lorenzo Longo*#

.4+|*18/05* 
|Samenkomst groep -> #*Groep*#

|Finaliseren applicatie -> #*Groep*#

|Verbeteren layout applicati -> #*Groep*#

|Testen eindapplicatie op testtablete -> #*Groep*#

.2+|*19/05* 
|Zoeken documenten voor documentatie -> #*Lorenzo Longo*#

|Uploaden documentatie etc op gitikdoeict -> #*Lorenzo Longo*#

.2+|*20/05* 
|Mailen Mnr. Mouton (extra info hardwarecomponenten) -> #*Lorenzo Longo*#

|Schrijven eindrapport (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

.2+|*21/05* 
|Nalezen en verbeteren teksten teamgenoten (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

|Verder schrijven eindrapport (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

.4+|*22/05* 
|Nalezen en verbeteren teksten teamgenoten (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

|Verder schrijven eindrapport (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*#

|Samenstellen onderdelen eindrapport (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*# - #*Jochen Duyck*# 

|Mailen eindrapport Mevr. Sabine Martens (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*# 

|*23/05* |Afwerken eindrapport (Wetenschappelijk rapporteren) -> #*Lorenzo Longo*# - #*Jochen Duyck*# 

|*28/05* |Mailen Mnr. Mouton (hardwarecomponenten ophalen) -> #*Lorenzo Longo*# 

.4+|*30/05* 
|Afhalen hardware (printer, tablet) -> #*Lorenzo Longo*# 

|Uittesten hardware -> #*Lorenzo Longo*# 

|Layout aanpassen van applicatie op tablet -> #*Lorenzo Longo*# 

|Uittesten applicatie met hardware -> #*Lorenzo Longo*# 

|*31/05* |Mailen Mnr. Mouton (update photobooth + info onkosten (inkt, case)) -> #*Lorenzo Longo*# 

|#*Juni*# | 

.3+|*01/06* 
|Mailen Mnr. Mouton (update bestelde materiaal + update opbouw case (met kostenraming)) -> #*Lorenzo Longo*# 

|Kopen materiaal case -> #*Lorenzo Longo*# 

|Mailen Mnr. Mouton (update prijs lamp) -> #*Lorenzo Longo*# 

|*06/06* |Verdeling werk voor presentatie -> #*Groep*# 

.2+|*07/06*
|Ophalen stekkerdoos -> #*Lorenzo Longo*# - #*Jochen Duyck*# 

|Ophalen lamp -> #*Lorenzo Longo*# - #*Jochen Duyck*# 

.2+|*10/06* 
|Bespreken opbouw presentatie -> #*Groep*#

|Ontwerpen van de presentatie -> #*Lorenzo Longo*#

.5+|*11/06*
|Afwerken presentatie -> #*Lorenzo Longo*#

|Schrijven tekst presentatie -> #*Lorenzo Longo*#

|Feedback tekst teamgenoten -> #*Lorenzo Longo*#

|Presentatie uploaden gitikdoeict -> #*Lorenzo Longo*#

|Updaten logboek -> #*Lorenzo Longo*#

.7+|*12/06* 
|Uittesten photobooth met houten behuizing -> #*Lorenzo Longo*# - #*Jochen Duyck*# 

|Verbeteren teksten teamgenoten (presentatie) -> #*Lorenzo Longo*# - #*Jochen Duyck*# 

|Router access point opstellen -> #*Lorenzo Longo*# - #*Jochen Duyck*# 

|Maken filmpje met werking photobooth -> #*Lorenzo Longo*# - #*Jochen Duyck*# 

|Verbeteren layout applicatie en testen -> #*Lorenzo Longo*# - #*Jochen Duyck*# 

|Updaten gitikdoeict documentatie -> #*Lorenzo Longo*# - #*Jochen Duyck*# 

|Updaten logboek -> #*Lorenzo Longo*#

|===
